# -------------------------------------------------------------------
# - NAME:        inputcheck.py
# - AUTHOR:      Reto Stauffer
# - DATE:        2015-01-16
# -------------------------------------------------------------------
# - DESCRIPTION:
# -------------------------------------------------------------------
# - EDITORIAL:   2015-01-16, RS: Created file on pc24-c707.
# -------------------------------------------------------------------
# - L@ST MODIFIED: 2015-01-17 23:52 on pc24-c707
# -------------------------------------------------------------------


# -------------------------------------------------------------------
# - Parsing input arguments
# -------------------------------------------------------------------
def inputcheck():

   import sys, os
   import utils
   import getopt
   from datetime import datetime as dt

   # ----------------------------------------------------------------
   # - Setting defaults first
   # ----------------------------------------------------------------
   inputs = {}
   inputs['date']       = None
   inputs['runhour']    = None
   inputs['userdate']   = None
   inputs['statlist']   = None

   short_opts = "d:r:s:h"
   long_opts  = ["date=","runhour=","statlist=""help"]
   try:
      opts, args = getopt.getopt(sys.argv[1:],short_opts,long_opts)
   except:
      usage('Got some unknown inputs. Read the usage.')

   # ----------------------------------------------------------------
   # - Take xyearmode (rouser) instead of imgi user?
   # ----------------------------------------------------------------
   xyearmode = False
   for rec in opts:
      o = rec[0]; v = rec[1] 
      print o, v
      if o in ['--help','-h']:
         usage()
      elif o in ['-d','--day']: 
         try:
            inputs['date'] = dt.strptime(v,'%Y%m%d')
         except:
            usage("Input argument -d/--day has had wrong format")
      elif o in ['-r','--runhour']:
         try:
            inputs['runhour'] = int(v)
         except:
            usage('Input argument -r/--runhour was no integer')
      elif o in ['-s','--statlist']:
         inputs['statlist'] = v
         
   # - runhour and date have to be set in pairs!
   if not inputs['date'] == None or not inputs['runhour'] == None:
      if inputs['date'] == None:
         usage('Input -r/--runhour set but not -d/--date. You have to set both!')
      elif inputs['runhour'] == None:
         usage('Input -d/--date set but not -r/--runhour. You have to set both!')

   # - Store a variable userdate which will be used
   #   later to check if the download for this date is allowed
   #   or not.
   if not inputs['date'] == None and not inputs['runhour'] == None:

      # - Runhour has to be 0, 6, 12, 18
      if not inputs['runhour'] in [0,6,12,18]:
         usage('Sorry, runhour has to be 0/6/12/18')

      date = dt.strftime(inputs['date'],'%Y%m%d')
      inputs['userdate'] = dt.strptime("%s%02d" % (date,inputs['runhour']),'%Y%m%d%H')

   return inputs


# -------------------------------------------------------------------
# - Shows usage and exits
# -------------------------------------------------------------------
def usage(msg=None):

   if not msg == None:
      print "\n\n   MESSAGE FROM SCRIPT:"
      print "   [!] %s" % msg

   print """
   Usage for the GENSvis.py script:

   Try to download latest run from the nomads servers:
   - GENSvis.py

   Specify own run (note: day AND runhour have to be set both):
   - GENSvis.py -d 20150115 -r 0
   - GENSvis.py -day 20150115 -runhour 00

   It is also possible to set a user specific statlist. In this case
   the script expects a file called XXXX_statlist.csv in the statlistdir
   specified in the config file. Stops if the file does not exist and
   gives you a hint why.
   The input on -s/--statlist is a character string - but not the full
   statlist file name but only the HASH leadint *_statlist.csv.
   - GENSvis.py -s IBK 
   - GENSvis.py --statlist IBK
   In this case the script is looking for IBK_statlist.csv.

   You can also specify to download a different resolution.
   The resolution will be checked later on. Allowed:
   0.25, 0.5, 1.0, and 2.5 at the moment:
   - GENSvis.py --resolution 0.25

   Inputs:
   -r/--runhour:  optional, integer. Allowed: 0/6/12/18.
   -d/--date:     optional, format YYYYMMDD
   -s/--statlist: optional, string. Station list hash. Not
                  full stationlist file name. Only the
                  hash followed by _statlist.csv.
   --resolution:  optional, floating point number.

   """

   import sys
   sys.exit(1)
