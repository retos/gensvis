# -------------------------------------------------------------------
# - NAME:        GENSdap.py
# - AUTHOR:      Reto Stauffer
# - DATE:        2015-01-15
# -------------------------------------------------------------------
# - DESCRIPTION:
# -------------------------------------------------------------------
# - EDITORIAL:   2015-01-15, RS: Created file on thinkreto.
# -------------------------------------------------------------------
# - L@ST MODIFIED: 2015-01-21 13:38 on pc24-c707
# -------------------------------------------------------------------

import pydap, sys, os
import numpy as np
from datetime import datetime as dt
import re
# - Setting timezone to UTC, just in case.
os.environ['TZ'] = 'UTC'



# -------------------------------------------------------------------
# - The worker I am using for the parallel downloading stuff.
# -------------------------------------------------------------------
def worker( args ):

   ## - Box has to have length 4
   param = args['param']
   minI = args['box'][0]
   maxI = args['box'][1]
   minJ = args['box'][2]
   maxJ = args['box'][3]
   tmin = args['t'][0]
   tmax = args['t'][1]
   mmin = args['m'][0]
   mmax = args['m'][1]
   data = args['data']

   # - If I/J is just the same: loading one point.
   #   If mmin and mmax are None, we are downloading the
   #   GFS where the member dimension is not defined.
   if mmin == None and mmax == None:
      print '    - PARALLEL DOWNLOADING, LOADING \"%s\": [%d:%d,%d:%d,%d:%d]' % \
         (param,tmin,tmax,minI,maxI,minJ,maxJ)
      # - The +1 is important to extend the range!
      #   1:3 is [1,2]. Therefore we have to load
      #   1:(3+1) to get [1,2,3]
      data = data[tmin:(tmax+1),minI:(maxI+1),minJ:(maxJ+1)]
      print '    - Data loaded for %s, data size: %s' % (param,str(data.shape))
   else:
      print '    - PARALLEL DOWNLOADING, LOADING \"%s\": [%d:%d,%d:%d,%d:%d,%d:%d]' % \
         (param,mmin,mmax,tmin,tmax,minI,maxI,minJ,maxJ)
      # - The +1 is important to extend the range!
      #   1:3 is [1,2]. Therefore we have to load
      #   1:(3+1) to get [1,2,3]
      data = data[mmin:(mmax+1),tmin:(tmax+1),minI:(maxI+1),minJ:(maxJ+1)]
   print '    - Data loaded for %s, data size: %s' % (param,str(data.shape))

   return {'param':param,'data':data}






