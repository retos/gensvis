# -------------------------------------------------------------------
# - NAME:        GFSdap.py
# - AUTHOR:      Reto Stauffer
# - DATE:        2015-01-15
# -------------------------------------------------------------------
# - DESCRIPTION:
# -------------------------------------------------------------------
# - EDITORIAL:   2015-01-15, RS: Created file on thinkreto.
# -------------------------------------------------------------------
# - L@ST MODIFIED: 2019-01-31 20:32 on marvin
# -------------------------------------------------------------------

import pydap, sys, os
import numpy as np
from datetime import datetime as dt
import utils
import re

# - Setting timezone to UTC, just in case.
os.environ['TZ'] = 'UTC'

# - Extending the GENERALdap class
from GENERALdap import GENERALdap


class GFSdap(GENERALdap):

   model = 'GFS'


   # ----------------------------------------------------------------
   # - Interpolating the data on 'data'. Checks if self.box exists.
   #   The data array dimension is different for the GEFS and the
   #   GFS and therefore the interpolate function is defined in here.
   # ----------------------------------------------------------------
   def interpolate(self,data,store=True):

      param = data['param']
      data  = data['data']

      if self.box == None or self.stations == None:
         sys.exit('You cannot interpolate data if %sdap.stations or %sdap.box missing') % \
                  (self.model,self.model)

      # - Create a new np.ndarray containing the interpolated
      #   values. Dimensions here are: (station,time)
      ip_data = np.ndarray( (len(self.stations),data.shape[0]), dtype='float' )

      # - Looping over all stations first, later over all
      #   members and time steps.
      for s in range(0,len(self.stations)):

         stat = self.stations[s]

         # - Grid size
         delta_i = np.abs(self.lat[0]-self.lat[1])
         delta_j = np.abs(self.lon[0]-self.lon[1])

         print '    Interpolation: cell size [deg] is: %.4f %.4f' % (delta_i,delta_j)

         # - If nn_lon/nn_lat length is just 2, take mean
         if len(stat['nn_lat']) == 2:
            i0 = stat['nn_lat'][0]; j0 = stat['nn_lon'][0] 
            i1 = stat['nn_lat'][1]; j1 = stat['nn_lon'][1] 
            corners = 2
         else:
            i0 = stat['nn_lat'][0]; j0 = stat['nn_lon'][0] 
            i1 = stat['nn_lat'][1]; j1 = stat['nn_lon'][1] 
            i2 = stat['nn_lat'][2]; j2 = stat['nn_lon'][2] 
            i3 = stat['nn_lat'][3]; j3 = stat['nn_lon'][3] 
            corners = 4

         # - Looping over time steps (t)
         for t in range(0,ip_data.shape[1]):

            #print data.shape
            #print ' V0 = data[%d,%d,%d]' % (t,i0-self.box[0],j0-self.box[2])
            #print ' V1 = data[%d,%d,%d]' % (t,i1-self.box[0],j1-self.box[2])

            # - Take data values
            V0 = data[t,i0-self.box[0],j0-self.box[2]]
            V1 = data[t,i1-self.box[0],j1-self.box[2]]

            if corners == 4:
               #print ' V2 = data[%d,%d,%d]' % (t,i2-self.box[0],j2-self.box[2])
               #print ' V3 = data[%d,%d,%d]' % (t,i3-self.box[0],j3-self.box[2])
               V2 = data[t,i2-self.box[0],j2-self.box[2]]
               V3 = data[t,i3-self.box[0],j3-self.box[2]]

               # - Compute the weights for the bilinar interpolation.
               #   Scetch: (for stations with 4 neighbors).
               #   The numbers are indizes of the defined objects
               #   and vectors.
               #   Furthermore, V0 corresponds to Corner (0), V1 to (1) and so on.
               #   WN, WS, WE, WW are the linear weights for North, South,
               #   East and West we need. self.dy and self.dx are the grid box
               #   width and height. The 'x' in the middle is the searched
               #   station location.
               #
               #    (3)                           (0)
               #       + ---------------------- +
               #       |                  |     |
               #       |                  |     |
               #       |                  |     |
               #       |               WN |     |
               #       |                  |     | self.dy
               #       |                  |     |
               #       |     WW           | WE  |
               #       |------------------x-----|
               #       |                  |     |
               #       |               WS |     |
               #       + ---------------------- +
               #   (2)        self.dx             (1)
               #

               WN = 1. - abs(self.lat[ stat['nn_lat'][0] ] - stat['lat']) / self.dy
               WS = 1. - abs(self.lat[ stat['nn_lat'][1] ] - stat['lat']) / self.dy
               WW = 1. - abs(self.lon[ stat['nn_lon'][2] ] - stat['lon']) / self.dx
               WE = 1. - abs(self.lon[ stat['nn_lon'][1] ] - stat['lon']) / self.dx

            # - Make the interpolation stuff
            if corners == 2:
               ip_data[s,t] = np.mean( [V0,V1] ) 
            else:
               ip_data[s,t] = (V0*WN+V1*WS)*WE + (V3*WN+V2*WS)*WW 

      # - If store is true, store to self.data_container
      if store:
         self.data_container[param] = ip_data

      return ip_data




   # ----------------------------------------------------------------
   # - GFS plot function
   # ----------------------------------------------------------------
   def __do_plot__(self,s,plotorder,typ):

      import matplotlib
      import matplotlib.pyplot as plt
      import RC_functions
      import RC_plotting
      import utils

      station = self.stations[s]

      # - Create image name
      stnname = utils.propper_station_short( station['name'] )
      imgname = '%s/%s_%s_%02d_%s.%s' % (self.config['img_dir'],self.model, \
             typ,int(self.initdate.strftime('%H')),stnname,self.config['img_postfix'])
      print '    - IMG name: %s' % imgname

      # - Setting some font sizes
      font_axt = self.config['img_axes_titlesize']
      font_xax = self.config['img_xtick_fontsize']
      font_yax = self.config['img_ytick_fontsize']

      matplotlib.rc('axes',  titlesize = font_axt )
      matplotlib.rc('xtick', labelsize = font_xax )
      matplotlib.rc('ytick', labelsize = font_yax )

      if typ == 'det':
         ##from RC_plotting import leps_plot as RCplot
         print '    ................. LOADING RC PLOTS HERE .................... '
      else:
         utils.exit('Sorry, for %s plot typ \"%s\" not allowed.' % (self.model,typ))

      # -------------------------------------------------------------
      # - Else prepare the data for the choosen station
      #   Prepare the data we need for using the GFS meteogram. 
      #   The structure has to be of the following:
      #   t = [0,...,T] is the number of time steps as set in self.time
      #
      #   y-axis: list of list containing the ensemble members.
      #   list(t):
      #     +- t=0: list
      #     +- t=1: list
      #     +- ...  ...
      #     +- t=T: list
      #
      #  x-axis list of time steps:
      #  list(t) = [fcsttime0,fcsttime1,...,fcsttimeT]
      # -------------------------------------------------------------

      # - Prepare the x axis (times) once, always stays the same. At
      #   the moment self.time is a np.ndarray.
      x = []
      for elem in self.time: x.append(elem)


      # - The interpolated values are in three dimensional np.ndarrays
      #   at the moment - each variable indicated by a key on
      #   self.data_container. Pick the current station and create
      #   one list-list object as described above for the station
      #   choosen in the input.
      y = {}; key_list = []
      for key in plotorder:
         print '   - Prepare %s' % key
         data = []
         for t in range(0,self.data_container[key].shape[1]):
            ##tmp = []
            ##for m in range(0,self.data_container[key].shape[2]):
            ##   # - Append forecast value to member list
            ##   tmp.append( self.data_container[key][s,t,m] )
            # - Append time list to data list
            ##data.append( tmp )
            data.append( self.data_container[key][s,t] )
         # - Append to y-dict now
         y[key] = data
         key_list.append(key)

      # - Open figure instance
      fig = plt.figure(figsize=(self.config['img_width'],self.config['img_height']))

      # -------------------------------------------------------------
      # - Computes how many axis we need. The GFS deterministic 
      #   plot routine is somehow handmade.
      # -------------------------------------------------------------
      # - Helperfunction checking if "key" is in the keys of 
      #   self.data_container
      # -------------------------------------------------------------
      def haskey(DC,key):
         if key in DC.keys(): return True
         return False
      DC = self.data_container
      n_axis = 0
      # - Plotting temperatures
      if haskey(DC,'tmp2m')  or haskey(DC,'dpt2m') or \
         haskey(DC,'tmax2m') or haskey(DC,'tmin2m'):
         n_axis = n_axis + 1
      # - Plotting wind speeds
      if haskey(DC,'ff10m') or haskey(DC,'ff100m') or haskey(DC,'gustsfc'):
         n_axis = n_axis + 1 
      # - Plotting wind direction 
      if haskey(DC,'dd10m') or haskey(DC,'dd100m') or haskey(DC,'gustsfc'):
         n_axis = n_axis + 1 
      # - Pressure
      if haskey(DC,'prmslmsl'):
         n_axis = n_axis + 1 
      # - Precipitation 
      if haskey(DC,'acpcpsfc') and haskey(DC,'apcpsfc'):
         n_axis = n_axis + 1 


      # - Prepare the axis
      margins, axis = self.__plot_prepare_axis__(fig,n_axis)


      # -------------------------------------------------------------
      # - Start plotting the figure now
      # -------------------------------------------------------------
      # - Current subplot
      sub = 0

      # -------------------------------------------------------------
      # - Plotting temperatures
      if haskey(DC,'tmin2m'):
         axis[sub].plot(x,y['tmin2m'],'bo')
      if haskey(DC,'tmax2m'):
         axis[sub].plot(x,y['tmax2m'],'ro')
      if haskey(DC,'tmp2m'):
         axis[sub].plot(x,y['tmp2m'],'r-',lw=2.)
      if haskey(DC,'dpt2m'):
         axis[sub].plot(x,y['dpt2m'],'g-',lw=2.)
      start, end = axis[sub].get_ylim()
      axis[sub].yaxis.set_ticks(np.arange(start, end+1, 2.))
      axis[sub].set_title("2m Temperature (current/min/max) and Dewpoint Temperature [C]",
         ha='left',position=(0,1),color='blue')
      sub = sub + 1


      # -------------------------------------------------------------
      # - Plotting wind speeds 
      if haskey(DC,'ff100m'):
         axis[sub].plot(x,y['ff100m'],'k--')
      if haskey(DC,'gustsfc'):
         axis[sub].plot(x,y['gustsfc'],'r-',lw=2.)
      if haskey(DC,'ff10m'):
         axis[sub].plot(x,y['ff10m'],'b-',lw=2.)
      start, end = axis[sub].get_ylim()
      axis[sub].yaxis.set_ticks(np.arange(start, end+1, 2.))
      axis[sub].set_title("10m Wind Speed and Gust Speed. Additional: 100m Wind Speed [m/s]",
         ha='left',position=(0,1),color='blue')
      sub = sub + 1

      # -------------------------------------------------------------
      # - Wind directions
      if haskey(DC,'dd100m'):
         axis[sub].plot(x,y['dd100m'],'gx')
      if haskey(DC,'dd10m'):
         axis[sub].plot(x,y['dd10m'],'ko')
      start, end = axis[sub].get_ylim()
      axis[sub].yaxis.set_ticks(np.arange(30., 331., 30.))
      axis[sub].set_ylim( 0., 360. ) 
      axis[sub].set_title("10m Wind Direction. Additional: 100m Wind Direction [deg]",
         ha='left',position=(0,1),color='blue')
      sub = sub + 1


      # -------------------------------------------------------------
      # - Surface pressure only 
      if haskey(DC,'acpcpsfc') and haskey(DC,'apcpsfc'):
         axis[sub].plot(x,y['acpcpsfc'],'g-')
         axis[sub].plot(x,y['apcpsfc'],'r-')
         axis[sub].set_title("Precipitation",
            ha='left',position=(0,1),color='blue')
         sub = sub + 1

      # -------------------------------------------------------------
      # - Surface pressure only 
      axis[sub].plot(x,y['prmslmsl'],'k-',lw=2.)
      axis[sub].set_title("Pressure Reduced to Mean Sea Level [deg]",
         ha='left',position=(0,1),color='blue')
      axis[sub].set_ylim( np.min(y['prmslmsl'])-1, np.max(y['prmslmsl'])+1 ) 
      sub = sub + 1

      ##print y.keys()

      ## - Plotting all the data
      #for i in range(0,len(self.data_container)):
      #   key = key_list[i]

      #   # - Searching for parameter configuration here
      #   #   Setting defaults and overwrite if possible.
      #   title    = key
      #   yticks   = 5
      #   if 'parameter_%s' % key in self.config.keys():
      #      settings = self.config['parameter_%s' % key]
      #      if 'title' in settings.keys():  title = settings['title']
      #      if 'yticks' in settings.keys(): yticks = settings['yticks']

      #   axis[i].plot( x, y[key] )
      #   start, end = axis[i].get_ylim()
      #   axis[i].yaxis.set_ticks(np.arange(start, end+1, yticks))
      #   axis[i].set_title(title,ha='left',position=(0,1),color='blue')
      #
      #   # - If ylim defined
      #   if 'parameter_%s' % key in self.config.keys():
      #      if 'ymax' in settings.keys() and 'ymin' in settings.keys():
      #         axis[i].set_ylim( settings['ymin'], settings['ymax'] )
      #      elif 'ymin' in settings.keys():
      #         axis[i].set_ylim( settings['ymin'], None )
      #      elif 'ymax' in settings.keys():
      #         axis[i].set_ylim( None, settings['ymax'] )


      # - Adding header and legend and stuff
      self.__plot_add_infos__(margins,typ,station)

      # - Save image to disc
      fig.savefig( imgname )
      plt.close()


   # ----------------------------------------------------------------
   # - Adding some infos to the plot
   # ----------------------------------------------------------------
   def __plot_add_infos__(self,typ,margins,station):

      import matplotlib
      import matplotlib.pyplot as plt
      import matplotlib.dates as dts

      # - Adding date to lowest x-axis
      xlab = dts.num2date(self.time[ 0]).strftime('%d %b %Y %HUTC')+" to "+ \
             dts.num2date(self.time[-1]).strftime('%d %b %Y %HUTC')
      plt.xlabel('\n'+xlab,color='blue')
    
      # - Prepare title strings
      loc = '%s (%.3fN, %.3fE, %dmasl)' % \
         (station['name'].upper(),station['lat'],station['lon'],station['height'])
      ####run = 'GFS Ensemble Distribution, %s %d %s' % \
      ####      ( dts.num2date(self.init).strftime('%A'),
      ####        int(dts.num2date(self.init).strftime('%d')),
      ####        dts.num2date(self.init).strftime('%B %Y %H UTC') )
      plt.figtext(0.95,0.99,self.latestrun.strftime('%A\n%d %B %Y %H UTC'),
          size=13,va='top',ha='right',color='blue')
      plt.figtext(0.05,0.99,"GFS High-Resolution Deterministic Meteogram\n%s" % (loc), \
          size=13,va='top',color='blue')

      # - Footer information
      if typ == 'ens':
         plt.figtext(0.5,margins[5]/3,'GFS Control Run',size=12,color='red',ha='center')
         plt.figtext(0.5,margins[5]/3-0.02,'Next 18Z-18Z Periode',size=12,color='gray',ha='center')
      # - Copyright
      info = []
      info.append( 'Model resolution: %.2f/%.2f degrees' % (self.dx,self.dy) )
      info.append( '2015: Reto Stauffer & Felix Oesterle' )
      for i in range(0,len(info)):
         plt.figtext(0.98,0.01 + (0.01*i),info[i],size=8,color='gray',ha='right')

      # - Adding some information about the used grid points and shit
      grid_info = []
      for n in range(0,len(station['nn_lon'])):
         grid_info.append('Gridpoint %s: %.2fN %.2fE' % \
            (n+1,self.lat[station['nn_lat'][n]],self.lon[station['nn_lon'][n]]))
      if len(station['nn_lon']) == 2:
         grid_info.append('Took neighbor mean value')
      else:
         grid_info.append('Four quadrant bilinear interpolation')
      for i in range(0,len(grid_info)):
         plt.figtext(0.05,0.01 + (0.01*i),grid_info[i],size=8,color='gray',ha='left')



   # ----------------------------------------------------------------
   # - Prepares the axis for the plot. Returns 
   #   list object containing the different axis where
   #   you can add the plots later on.
   # ----------------------------------------------------------------
   def __plot_prepare_axis__(self,fig,nsub):

      import matplotlib
      import matplotlib.dates as dts
   
      # - RS: Subplot margins and plot margings
      margins = [0.035,0.02,0.05,0.02,0.02,0.05] # header space, bottom, left, top, right, footer space

      # - Some axis configs we need 
      days    = dts.DayLocator( interval = 1 )
      hours   = dts.HourLocator( byhour= [12] )
      daysFmt = dts.DateFormatter('%a %d') 


      # - Compute position for each of the subplots
      axis = []
      for i in range(0,nsub):
         pos = [margins[2],                                                        # left
                (1.-margins[0]-margins[5])/nsub*(nsub-i-1)+margins[1]+margins[5],  # bottom
                 1.-margins[2]-margins[4],                                         # width
                (1.-margins[0]-margins[5])/nsub-margins[1]-margins[3]]             # height
         
         axis.append( fig.add_subplot(nsub,i+1,1,position=pos) )

         # - Adding axis properties
         axis[i].xaxis.set_major_locator( days )
         axis[i].xaxis.set_minor_locator( hours )
         axis[i].xaxis.set_major_formatter( matplotlib.ticker.NullFormatter() )
         axis[i].xaxis.set_minor_formatter( daysFmt )
         for tick in axis[i].xaxis.get_minor_ticks():
           tick.tick1line.set_markersize(0)
           tick.tick2line.set_markersize(0)
           tick.label1.set_horizontalalignment('center')
         axis[i].xaxis.grid(color='k', linestyle=':', linewidth=0.3)
         axis[i].yaxis.grid(color='k', linestyle=':', linewidth=0.3)

      return margins,axis


   # ----------------------------------------------------------------
   # - Write data to an ASCII file
   #   If input "param" is None write all data in self.data_container
   #   to the file. Else only the one you specified.
   # ----------------------------------------------------------------
   def write_asciifiles(self,param=None):

      if self.config['datadir'] == None:
         print "   ASCII output mode disabled, return."
         return

      # - If param is none: take all available parameters.
      if param == None:
         param = self.data_container.keys()
      # - If set, only take the ones which are set. If the
      #   input param is a string: convert to list.
      elif type(param) == type(str()):
         param = [param]

      # - Looping over all stations
      for s in range(0,len(self.stations)):

         station = self.stations[s]

         # - Propper name for the ascii file now
         stnname = utils.propper_station_short( station['name'] )
         fname = '%s/%s_%02d_%s.dat' % (self.config['datadir'],self.model, \
                int(self.initdate.strftime('%H')),stnname)
         print '    - ASCII file name: %s' % fname

         # ----------------------------------------------------------
         # - Open file, no append mode. Write header which also
         #   overwrites existing content.
         # ----------------------------------------------------------
         fid = open(fname,'w')
         fid.write('# %s\n' % 'ASCII output from GFSvis.py')
         fid.write('# %s\n' % 'Contains interpolated values from the')
         fid.write('# %s\n' % 'GFS deterministic forecast on %f x %f degrees' % (self.dx,self.dy) )
         fid.write('# %s\n' % 'The data in here are for station')
         fid.write('# %d: %s\n' % (station['stnr'],station['name']))
         fid.write('# %s  %7.4f %7.4f\n' % ('Station position:',station['lon'],station['lat']))
   
         # - Info about the used neighbor grid points
         for i in range(0,len(station['nn_lon'])):
            fid.write('# Used grid point %d: %7.4f %7.4f\n' % \
               (i+1,self.lon[ station['nn_lon'][i] ],self.lat[ station['nn_lat'][i] ]))

         # - Variable description
         fid.write('%-12s %-4s ' % ('timestamp','step') )
         for key in param: 
            fid.write('%-10s ' % key)
         fid.write('\n')

         # - Looping trough the variables. If input "param" is set
         #   skip all keys not in param.
         for t in range(0,len(self.time)):

            datetime = self.__time_to_datetime__(self.time[t])
            step = (datetime - self.initdate).days*24 + (datetime - self.initdate).seconds/3600.
            fid.write( "%12d %4d" % (int(datetime.strftime('%s')),step) )

            for key in param: 
               fid.write('%10.3f' % self.data_container[key][s,t])
            fid.write('\n')






