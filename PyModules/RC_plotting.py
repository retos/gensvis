#/usr/bin/python
#---------written by Felix Oesterle (FSS)-----------------
# -DESCRIPTION:
#   Plotting functions for  RainCloud workflow
# -TODO:
# -Last modified:  Thu Mar 14, 2013  15:01
#@author Felix Oesterle
#--------------------------------------
import os
from subprocess import *
import sys
import datetime
import pickle
#import Gnuplot
import numpy as np
import RC_functions as RCf
#from PyModules import RC_functions as RCf
#from PyDISS import PlotPS
import time
import matplotlib
import matplotlib.dates as dts
matplotlib.use('Agg') 
#matplotlib.use('GTKAgg') 
import matplotlib.pyplot as plt
#from mpl_toolkits.basemap import Basemap, cm

from scipy.stats import scoreatpercentile
from scipy.stats.mstats import mquantiles
#import netCDF4
#import json

def leps_plot_gen_data(data,pos):
    """
    generate data for leps_plot
    """
    from scipy.stats import scoreatpercentile
    from scipy import interpolate

    
    perc =list()
    for d,p in zip(data,pos):
        # percentiles of interest
        perc.append([min(d), scoreatpercentile(d,10), scoreatpercentile(d,25),
                       scoreatpercentile(d,50), scoreatpercentile(d,75),
                       scoreatpercentile(d,90), max(d)])
     
    perc = np.asarray(perc)
    return perc

def leps_plot(ax,data,pos):
    '''
    create CosmoLEPS like plot
    '''
    perc = leps_plot_gen_data(data,pos)
    
    # FSS---minius and maximus 
    #xn =np.linspace(pos[0],pos[-1],100)
    #f = interpolate.interp1d(pos,perc[:,0],2)
    #f = interpolate.interp1d(pos,perc[:,0],'quadratic')
    #smootch=f(xn)
    #ax.plot(xn,smootch,color='0.5',linestyle='dashed',linewidth=2)
    ax.plot(pos,perc[:,0],pos,perc[:,-1],color='0.5',linestyle='dashed',linewidth=2)

    # FSS---median
    ax.plot(pos,perc[:,3],'r',lw=2)
    
    # FSS---10 - 90 perc 
    ax.fill_between(pos,perc[:,1],perc[:,-2],facecolor='gray',color='gray',alpha=0.2)
    #ax.fill_between(pos,perc[:,1],perc[:,-2],facecolor='lightgray',color='lightgray',alpha=0.2)
    
    # FSS---25 - 75 perc 
    ax.fill_between(pos,perc[:,2],perc[:,4],facecolor='gray',color='gray',alpha=0.3)
    #ax.set_ylim(bottom =-4)


def violin_plot(ax,data,pos, bp=False,width=1.0):
    '''
    create violin plots on an axis
    '''
    from numpy import arange
    from scipy.stats import gaussian_kde
    dist = max(pos)-min(pos)
    w = min(0.15*max(dist,1.0),0.5)
    for d,p in zip(data,pos):
        k = gaussian_kde(d) #calculates the kernel density
        m = k.dataset.min() #lower bound of violin
        M = k.dataset.max() #upper bound of violin
        x = arange(m,M,(M-m)/100.) # support for violin
        v = k.evaluate(x) #violin profile (density curve)
#        v = width*v/v.max()*w #scaling the violin to the available space
        v = width*v/0.7*w #scaling the violin so that all scale to same
        ax.fill_betweenx(x,p,v+p,facecolor='cyan',alpha=0.3)
        ax.fill_betweenx(x,p,-v+p,facecolor='cyan',alpha=0.3)
    if bp:
        ax.boxplot(data,notch=1,positions=pos,vert=1)


def eps_plot(ax,data,pos,width=0.3,bcolor='#9EC9F7'):
    '''
    create ecmwf eps like plot 
    '''
    from scipy.stats import scoreatpercentile
    from matplotlib.patches import Polygon
    from matplotlib.patches import Patch 
    import numpy as np
    
    dist = max(pos)-min(pos)
    w = width*min(0.15*max(dist,1.0),0.5)

   
    for d,p in zip(data,pos):
        # percentiles of interest
        perc = [min(d), scoreatpercentile(d,10), scoreatpercentile(d,25),
                       scoreatpercentile(d,50), scoreatpercentile(d,75),
                       scoreatpercentile(d,90), max(d)]
        midpoint = p # time-series time
        
        #v = width*v/v.max()*w #scaling the violin to the available space
        # min/max
        ax.broken_barh([(midpoint-.01*w,.02*w)], (perc[0], perc[1]-perc[0]))
        ax.broken_barh([(midpoint-.01*w,.02*w)], (perc[5], perc[6]-perc[5]))
        # 10/90
        ax.broken_barh([(midpoint-.15*w,.3*w)], (perc[1], perc[2]-perc[1]),facecolor=bcolor)
        ax.broken_barh([(midpoint-.15*w,.3*w)], (perc[4], perc[5]-perc[4]),facecolor=bcolor)
        ## 25/75
        ax.broken_barh([(midpoint-.4*w,.8*w)], (perc[2], perc[3]-perc[2]),facecolor=bcolor)
        ax.broken_barh([(midpoint-.4*w,.8*w)], (perc[3], perc[4]-perc[3]),facecolor=bcolor)

    # 2013-03-15, RS: Added control run plot
    ycontrol = []
    for i in range(0,len(data)):
      ycontrol.append(data[i][0])
    ax.plot(pos,ycontrol, 'r-', lw=1)

    # - Adding "next wb forecast periode" as shaded background
    wbpos = ( np.floor(min(pos))+18/24.,
              np.floor(min(pos))+18/24.,
              np.floor(min(pos))+42/24.,
              np.floor(min(pos))+42/24. )
    #print wbpos
    wby   = ( min(min(data)),
              max(max(data)),
              max(max(data)),
              min(min(data)) )
    ylim  = ax.axis()
    wby   = ( ylim[2],ylim[3],ylim[3],ylim[2] ) 
    wb    = Polygon( zip(wbpos,wby), edgecolor='none', facecolor='0.2', alpha=0.1, zorder=-1) 
    ax.add_patch(wb)

def PPF_perc_to_netcdf_and_plots(times,precip,lat,lon,n_per_time,base_nc,RCenv,info,packlist,plots=False):
    '''
    percentiles to netcdf 
    '''

    print "Dimension lon", len(lon) 
    print 'Dimension lat', len(lat) 
    print 'Dimension precip_dat first', len(precip)

    # FSS---condense array 
    # precip[timestep][run][lat,lon]
    times,precip = RCf.CondenseArray(times,precip)

    print "Dimension Time", len(times)
    #print "Dimension runs", len(precip[0])
    #print "Dimension runs", len(precip[1])
    #print "Dimension runs", len(precip[2])
    print 'Dimension precip after', len(precip)
    
    # FSS---get percentiles for each timestep 
    #  mquantiles can handle 2D arrays at most, so splitting is needed
    #meds = np.empty(shape=(yn_a[:,1,:,:].shape))
    meds = list()
    tens = list()
    nines= list()

    # FSS--- Loops for quantile computations 
    # TODO these loops seem verrrrry inefficient 
    #!!!!!!!!!!!!!!!!!!!!!!
    #print "!!!!!!!!!!!!!!!!!!fixed n_per_time"
    #n_per_time = 3
    for el in precip:
        el = np.array(el)

        # FSS---extend the data to contain n_per_time layers (fill with 0's) 
        missing_layers = n_per_time - el.shape[0]
        if missing_layers > 0:
            el2 = np.zeros(shape=(missing_layers,el.shape[1],el.shape[2]))
            el = np.concatenate((el,el2), axis = 0)
        
        med_si = np.empty(shape=(el[0,:,:].shape))
        tens_si = np.empty(shape=(el[0,:,:].shape))
        nines_si = np.empty(shape=(el[0,:,:].shape))
        
        for i in range(el.shape[2]): #handling each lon slice seperately, as mquantiles is only 2D
            m_tmp = mquantiles(el[:,:,i],prob=[0.1,0.5,0.9],axis=0)
            tens_si[:,i] = m_tmp[0]
            med_si[:,i] = m_tmp[1]
            nines_si[:,i] = m_tmp[2]

        meds.append(med_si)  
        tens.append(tens_si)
        nines.append(nines_si)

       
    meds_arr = np.array(meds)
    tens_arr = np.array(tens)
    nines_arr= np.array(nines)

    #print times

    # FSS---create netcdf  
    f = netCDF4.Dataset(base_nc,'r')
    fon = RCenv['bashdir']+'/work/Percentiles_'+str(info['ecrun'])+'.nc'
    fout = netCDF4.Dataset(fon,'w',format=f.file_format)
    
    unlimdim,unlimdimname = RCf.CopyBaseInfo_Netcdf(f,fout)
  
    # FSS---clipping takes away values below 0 
    #RCf.CopyVarAndAttr_Netcdf(f,fout,'precip-fft-dl',unlimdim,unlimdimname,clipping=True)
    RCf.CopyVarAndAttr_Netcdf(f,fout,'lat',unlimdim,unlimdimname)
    RCf.CopyVarAndAttr_Netcdf(f,fout,'lon',unlimdim,unlimdimname)
 
    ncvar = f.variables['precip-fft-dl']

    #if unlimdimname and unlimdimname in ncvar.dimensions:
        #hasunlimdim = True
    #else:
        #hasunlimdim = False

    fout.createDimension('time', None)
    time2 = fout.createVariable('time','f8',('time',))
    time2[:] = times
    
    tens = fout.createVariable('tens',ncvar.dtype,('time','lat','lon'),zlib=True) 
    tens.setncatts(ncvar.__dict__)
    tens[:] = tens_arr[:,:,:] 
    meds = fout.createVariable('median',ncvar.dtype,('time','lat','lon'),zlib=True) 
    meds.setncatts(ncvar.__dict__)
    meds[:] = meds_arr[:,:,:] 
    nines = fout.createVariable('nines',ncvar.dtype,('time','lat','lon'),zlib=True) 
    nines.setncatts(ncvar.__dict__)
    nines[:] = nines_arr[:,:,:] 
    
    fout.sync()
    f.close()
    fout.close()

    jsonlist = dict()
    # FSS---PLOTS
    if plots:
        print "plots requested"
        # FSS---levels for contours 
        levels = [0.1, 0.5, 1,2,3,4,5,7,10]
        parallels = np.arange(40,50,0.5)
        meridians = np.arange(5,20,0.5)

        x2,y2 = np.meshgrid(lon,lat)
        
        for i,ti in enumerate(times):
            print i
            valid_time =  dts.num2date(ti).strftime("%Y-%m-%d-%H-%M")
            fig = plt.figure(figsize=(10,6))
            ##m = Basemap(llcrnrlon=11.0,llcrnrlat=47.0,urcrnrlon=12.3,urcrnrlat=48.1,
            m = Basemap(llcrnrlon=10.1,llcrnrlat=46.1,urcrnrlon=13.2,urcrnrlat=48.,
            ##resolution='h',projection='tmerc',lon_0=15.,lat_0=78)
            resolution='i',projection='cyl',lon_0=12.,lat_0=46)


            ax = fig.add_subplot(1,1,1) 
            m.drawcoastlines()
            m.drawcountries(linewidth=1.5)
            m.drawrivers(color='blue',linewidth=0.2)
            #m.shadedrelief()
            ##v = np.linspace(0.1, 20.0, 15, endpoint=True)
            #cs = m.contourf(x2,y2,topo,cmap=plt.get_cmap("gist_yarg"),alpha=0.8)
            ##cs2 = m.contourf(x2,y2,myval,v,cmap=theCM)
            ##cs2 = m.contourf(x2,y2,myval,levels,cmap=theCM,vmin=0.1,vmax=8)
            cs2 = m.contourf(x2,y2,nines_arr[i,:,:],levels,alpha=0.4)
            ##cs2 = m.contourf(x2,y2,myval,cmap=plt.get_cmap("Winter"),alpha=0.6)
            m.drawparallels(parallels,labels=[False,True,True,False])
            m.drawmeridians(meridians,labels=[True,False,False,True])
            cax = fig.add_axes([0.92, 0.2, 0.01, 0.6])
            cbar = plt.colorbar(cs2,cax=cax)
            #ax.set_title('10')
            ##cs2.set_clim(0,8)

            #ax = fig.add_subplot(1,3,2) 
            #m.drawcoastlines()
            #m.drawcountries(linewidth=1.5)
            #m.drawrivers(color='blue',linewidth=0.2)
            #cs = m.contourf(x2,y2,topo,cmap=plt.get_cmap("gist_yarg"),alpha=0.8)
            #cs2 = m.contourf(x2,y2,ma[-1],levels,alpha=0.4)
            #m.drawparallels(parallels,labels=[False,True,True,False])
            #m.drawmeridians(meridians,labels=[True,False,False,True])
            #ax.set_title('Median')


            #ax = fig.add_subplot(1,3,3) 
            #m.drawcoastlines()
            #m.drawcountries(linewidth=1.5)
            #m.drawrivers(color='blue',linewidth=0.2)
            #cs = m.contourf(x2,y2,topo,cmap=plt.get_cmap("gist_yarg"),alpha=0.8)
            #cs2 = m.contourf(x2,y2,na[-1],levels,alpha=0.4)
            #m.drawparallels(parallels,labels=[False,True,True,False])
            #m.drawmeridians(meridians,labels=[True,False,False,True])
            #ax.set_title('90')

            ## Make an axis for the colorbar on the right side
            #fig.suptitle('precip rate [mm/h] ')
            #plt.draw()
            #plt.savefig('visu_output/Maps_percentile.png')
            basen = 'prec_nines_'+str(info['ecrun'])+'_'+valid_time+'.png'
#            jsonlist.append('plots/'+basen) # defines folder on webserver (for dissemination)
            if "nines" not in jsonlist:
                jsonlist["nines"] = dict()
            jsonlist["nines"][i] = dict()
            #jsonlist["nines"].append('plots/'+basen) # defines folder on webserver (for dissemination)
            jsonlist["nines"][i]['name']='plots/'+basen # defines folder on webserver (for dissemination)
            jsonlist["nines"][i]['time']= valid_time # defines folder on webserver (for dissemination)
            pltname = RCenv['bashdir']+'/work/'+basen
           
            
            print pltname
            plt.savefig(pltname)
            packlist.append(pltname)


    jsonname = RCenv['bashdir']+'/work/imageinfo.json'
    with open(jsonname, 'w') as outfile:
        json.dump(jsonlist, outfile)
    packlist.append(jsonname)

    packlist.append(fon)

def PPF_leps_plot(xval,yval,packlist,stationfile,n_per_time,RCenv,info):
    '''
    big wrapper for leps plot called from PPF
    '''
    
    # FSS---getting station info 
    dt = (int,float,float,'|S30')
    stations = np.genfromtxt(stationfile, names=True, dtype = dt)

    # FSS---Prepare data dictionary which is used for station data
    data_dict = {} #dict
    for station in stations:
        data_dict[station['id']] = [] #list
    
    # FSS---one element in yval represents one LM run
    #yv2 = list()
    for valid,item in zip(xval,yval):
        b = dict(item)
        #TODO 
        # Handle more than nmax values ... (overlapping grids)
        for key in data_dict.iterkeys():
            try:
                data_dict[key].append(b[key])
            except KeyError:
                continue
        #yv2.append(b[197058])
    

    #for key in data_dict.iterkeys():
        #print data_dict[key]
 
    #for key in data_dict.iterkeys():
        #print key, data_dict[key]
  

    # FSS--- convert to array and condense 
    # check arrays: if all are empty, then station wasn't found at all-> no data
    # if only some are empty -> fill with zeros
    for key in data_dict.iterkeys():
        data_dict[key] = np.array(data_dict[key])
        if any(data_dict[key]):
            data_dict[key][np.isnan(data_dict[key])] = 0
        
        xn,data_dict[key] = RCf.CondenseArray(xval,data_dict[key]) 
   
    # FSS---fill all times with 0 up to n_per_time (just a safety measure) 
    for key in data_dict.iterkeys():
        if not any(data_dict[key]): #data is completely empty
            continue
        for i,v in enumerate(data_dict[key]):
            if len(v) < n_per_time:
                data_dict[key][i] = data_dict[key][i] + [0.0]*(n_per_time - len(data_dict[key][i]))
    
    #yval = np.array(yv2)
    #xn,yn = RCf.CondenseArray(xval,yval)

    #for key in data_dict.iterkeys():
        #print data_dict[key]
    #print "xn", xn
    #print "yn", yn
    #print "dd", data_dict[197058]

    
    # FSS---Leps like plot 
    auto = dts.AutoDateLocator() 
    hours = dts.HourLocator(byhour=range(6,24,6))
    days = dts.DayLocator(interval=1)
    daysFmt = dts.DateFormatter('%d/%m %H')
    hoursFmt = dts.DateFormatter('%H')
    

    # FSS---Time needed for PostprocessFinal (s) 4.9 
    fig = plt.figure(figsize=(10,4))
    ax = fig.add_subplot(111)
    ax.grid(True)
    ax.xaxis.set_major_locator(days)
    ax.xaxis.set_major_formatter(daysFmt)
    ax.xaxis.set_minor_locator(hours)
    ax.xaxis.set_minor_formatter(hoursFmt)
    ax.set_ylim([0,6])
    plt.ylabel('precip rate [mm/h]')
    plt.xlabel('date')
    i = 1
    for key in data_dict.iterkeys():
        # FSS---check if all values are empty 
        if not any(data_dict[key]):
            continue
        plt.title('min 25 50 75 max for '+str(key))
    
        perc = leps_plot_gen_data(data_dict[key][:],xn)
        
        if i == 1: #First full plot
            ## FSS---minimus and maximus 
            linemax, = ax.plot(xn,perc[:,-1],color='0.5',linestyle='dashed',linewidth=2)
            linemin, = ax.plot(xn,perc[:,0],color='0.5',linestyle='dashed',linewidth=2)
            # FSS---median
            line1, = ax.plot(xn,perc[:,3],'r',lw=2)
            # FSS---10 - 90 perc 
            pc2 = ax.fill_between(xn,perc[:,1],perc[:,-2],facecolor='lightgray',color='lightgray',alpha=0.2)
            # FSS---25 - 75 perc 
            pc = ax.fill_between(xn,perc[:,2],perc[:,4],facecolor='gray',color='gray',alpha=0.3)
            i= i+1
            ## FSS---develop
            #line2 = ax.plot(xn,data_dict[key][:],'ro')
        else: #after first: only update y values
            line1.set_ydata(perc[:,3])
            linemax.set_ydata(perc[:,-1])
            linemin.set_ydata(perc[:,0])
            pc2.remove()
            pc2 = ax.fill_between(xn,perc[:,1],perc[:,-2],facecolor='lightgray',color='lightgray',alpha=0.2)
            pc.remove()
            pc = ax.fill_between(xn,perc[:,2],perc[:,4],facecolor='gray',color='gray',alpha=0.3)
            #for line in line2:
                #line.remove()
            #line2 = ax.plot(xn,data_dict[key][:],'ro')
            fig.canvas.draw()

        pltname = RCenv['bashdir']+'/work/leps_'+str(key)+'_'+str(info['ecrun'])+'.png'
        plt.savefig(pltname)
        packlist.append(pltname)

    return packlist
    










