#/usr/bin/python
#---------written by Felix Oesterle (FSS)-----------------
# -DESCRIPTION:
#   Functions for  RainCloud workflow
# -TODO:
# -Last modified:  Thu Feb 21, 2013  11:57
#@author Felix Oesterle 
#--------------------------------------
import os
import tarfile
import sys
#import netCDF4
import string
import numpy as np

def CopyBaseInfo_Netcdf(fin,fout):
    
    fout.setncatts(fin.__dict__)
    
    unlimdimname = False
    unlimdim = None

    for dimname,dim in fin.dimensions.items():
       if dim.isunlimited():
           unlimdimname = dimname
           unlimdim = dim
           fout.createDimension(dimname,None)
       else:
           fout.createDimension(dimname,len(dim)) 

    fout.sync()
    return unlimdim, unlimdimname

def CopyVarAndAttr_Netcdf(fin,fout,varname,unlimdim,unlimdimname,clipping=False):
    """
        Clipping removes values below 0
    """
 #for varname,ncvar in  f.variables.items():
    ncvar = fin.variables[varname]

    if unlimdimname and unlimdimname in ncvar.dimensions:
        hasunlimdim = True
    else:
        hasunlimdim = False

    new = fout.createVariable(varname,ncvar.dtype,ncvar.dimensions) 
    new.setncatts(ncvar.__dict__)

    if hasunlimdim:
        if clipping:
            new[0:len(unlimdim)] = np.clip(ncvar[:],0.0,max(ncvar[:].flatten()))
        else:
            new[0:len(unlimdim)] = ncvar[:]
    else:
        if clipping:
            new[:] = np.clip(ncvar[:],0.0,max(ncvar[:].flatten()))
        else:
            new[:] = ncvar[:]

    fout.sync()


def PackActivityFiles(infiles,tarname):
    #print "Packing "+tarname
    tar = tarfile.open(tarname, "w:gz")    
    nmlfiles = list()
    for element in infiles:
        try:
#            f = open(element, 'r')
#           'a' defines to touch a file if it doesn't exist
            f = open(element, 'a')
            tar.add(element)
            f.close()
            os.remove(element)
        except IOError:
            print "nothing to add"

    tar.close()

def UnpackActivityFiles(tarname,delflag="delete"):
   # read the tarfile
    #print "Unpacking "+tarname
    tar = tarfile.open(tarname, "r:gz")
    
    # another way to get the file list
    file_list = tar.getnames()

    #tar.extractall()
    map(tar.extract,tar.getmembers()) 
    tar.close()

    if delflag=="delete":
        os.remove(tarname)
    return file_list

def CreatePacklist(element,basename):
    packfiles=list()
    tarname = ""
    for key in element.keys():
        if key == 'uid':
            tarname = "./work/"+basename+"_uid"+element['uid']+".tar.gz" 
        else:
            packfiles.append(element[key])

    return packfiles,tarname

def GetUid(file):
    uid = file.split("uid")[1]
    all=string.maketrans('','')
    nodigs=all.translate(all, string.digits)
    uid = uid.translate(all, nodigs)
    return uid

def GetFileAndUid(filelist,extension):
    nmlfile=""
    #print extension  
    #print filelist
    for element in filelist:
        if extension in os.path.splitext(element)[1]:
            nmlfile = element
            uid = GetUid(element) 
    
    return nmlfile,uid

def CleanFiles(file_list):
    print "Cleaning"
    for element in file_list:
        try:
            os.remove(element)
        except OSError:
            continue

def PrintDU(folder):
    folder_size = 0
    for (path, dirs, files) in os.walk(folder):
        for file in files:
            filename = os.path.join(path, file)
            folder_size += os.path.getsize(filename)
     
    print "-"*10
    print "Folder "+folder+" = %0.4f MB" % (folder_size/(1024*1024.0))
    print "-"*10


def CondenseArray(x,y):
    # FSS--- A[0,0,0,6,6,9] B[1,1,1,2,2,3,3] -> [0,6,9] [[1,1,1],[2,2],[3,3]]
    xnew = list()
    ynew = list()

    # FSS---make x unique  
    xnew = unique_order_pres(x) 

    for item in xnew:
        # FSS---filter items  
        msk = [(el==item) for el in x] 
        ynew.append([y[i] for i in xrange(len(y)) if msk[i]])

    return xnew, ynew

def unique_order_pres(seq, idfun=None): 
    # order preserving
    if idfun is None:
        def idfun(x): return x
    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        # in old Python versions:
        # if seen.has_key(marker)
        # but in new ones:
        if marker in seen: continue
        seen[marker] = 1
        result.append(item)
    return result   


#def nc4tonc3(filename4,filename3,clobber=False,nchunk=10,quiet=False,format='NETCDF3_64BIT'):
    #"""convert a netcdf 4 file (filename4) in NETCDF4_CLASSIC format
    #to a netcdf 3 file (filename3) in NETCDF3_64BIT format."""
    #ncfile4 = Dataset(filename4,'r')
    #if ncfile4.file_format != 'NETCDF4_CLASSIC':
        #raise IOError('input file must be in NETCDF4_CLASSIC format')
    #ncfile3 = Dataset(filename3,'w',clobber=clobber,format=format)
    ## create dimensions. Check for unlimited dim.
    #unlimdimname = False
    #unlimdim = None
    ## create global attributes.
    #if not quiet: sys.stdout.write('copying global attributes ..\n')
    ##for attname in ncfile4.ncattrs():
    ##    setattr(ncfile3,attname,getattr(ncfile4,attname))
    #ncfile4.setncatts(ncfile3.__dict__) 
    #if not quiet: sys.stdout.write('copying dimensions ..\n')
    #for dimname,dim in ncfile4.dimensions.items():
        #if dim.isunlimited():
            #unlimdimname = dimname
            #unlimdim = dim
            #ncfile3.createDimension(dimname,None)
        #else:
            #ncfile3.createDimension(dimname,len(dim))
    ## create variables.
    #for varname,ncvar in ncfile4.variables.items():
        #if not quiet: sys.stdout.write('copying variable %s\n' % varname)
        ## is there an unlimited dimension?
        #if unlimdimname and unlimdimname in ncvar.dimensions:
            #hasunlimdim = True
        #else:
            #hasunlimdim = False
        #if hasattr(ncvar, '_FillValue'):
            #FillValue = ncvar._FillValue
        #else:
            #FillValue = None 
        #var = ncfile3.createVariable(varname,ncvar.dtype,ncvar.dimensions,fill_value=FillValue)
        ## fill variable attributes.
    #attdict = ncvar.__dict__
    #if '_FillValue' in attdict: del attdict['_FillValue']
    #var.setncatts(attdict)
        ##for attname in ncvar.ncattrs():
        ##    if attname == '_FillValue': continue
        ##    setattr(var,attname,getattr(ncvar,attname))
        ## fill variables with data.
        #if hasunlimdim: # has an unlim dim, loop over unlim dim index.
            ## range to copy
            #if nchunk:
                #start = 0; stop = len(unlimdim); step = nchunk
                #if step < 1: step = 1
                #for n in range(start, stop, step):
                    #nmax = n+nchunk
                    #if nmax > len(unlimdim): nmax=len(unlimdim)
                    #var[n:nmax] = ncvar[n:nmax]
            #else:
                #var[0:len(unlimdim)] = ncvar[:]
        #else: # no unlim dim or 1-d variable, just copy all data at once.
            #var[:] = ncvar[:]
        #ncfile3.sync() # flush data to disk
    ## close files.
    #ncfile3.close()
    #ncfile4.close()





























