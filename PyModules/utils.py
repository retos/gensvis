



# - Reading the stationdata file
def read_statlist(filename):

   import fileinput

   print(" *  Reading station list")
   print("    Location: " + filename)

   stations = [] 
   #stations = {'stnr':[],'name':[],'lon':[],'lat':[]}

   lines=([])
   for line in fileinput.input([filename]):
      lines.extend([line])

   # - Looping trough lines
   for line in lines:
      if line.startswith("#"):
         continue
      if line.strip().__len__() == 0:
         continue

      parts = line.split(";")
      # - Grab different elements and convert to correct data type
      stnr     = int(parts[0].strip())
      name     = str(parts[1].strip())
      name     = name.replace(" ","-")
      lat      = float(parts[2].strip())
      lon      = float(parts[3].strip())
      hgt      = float(parts[4].strip())
      mode     = int(parts[5].strip())
      project  = str(parts[6].strip())

      if lon < 0:
         print '    - Adding 360 degrees to longitude %s here ' % lon
         lon = lon + 360

      stations.append( {'stnr':stnr,'name':name,'lon':lon,'lat':lat,'height':hgt} )

   if len(stations) == 0:
      exit('STOP: no stations found. Stop the script here.')


   return stations

# -------------------------------------------------------------------
# - Some stations have fancy definitions with '/', '-' and '('
#   and stuff. If '/-(' is present, take the part before only. 
# -------------------------------------------------------------------
def propper_station_short( name ):

   name = name.strip().replace(' ','_')
   for i in ['/','(',')','\'','\"']:
      name = name.split(i)[0]

   return name.lower()


# -------------------------------------------------------------------
# - Searching for the four nearest grid points - one in each
#   quadrant. We need them later to define the area we have to
#   download and to make the bilinar interpolation.
# -------------------------------------------------------------------
def nearest(obj,s):

   stationlon = obj.stations[s]['lon']
   stationlat = obj.stations[s]['lat']
   name       = obj.stations[s]['name']

   # If longitude is defined as 'West': switch to 'East'
   if stationlon < 0: stationlon = 360. + stationlon

   print("    - Searching nearest points for {0:s}".format(name))
   print("      Coordinates (lat/lon): {0:.2f} {1:.2f}".format(stationlat,stationlon))

   import numpy as np

   # - Searches for nearest neighbor on the negative side
   def nnpositive(coord, stn):
      res = 999.
      coord = coord - stn
      for c in coord:
         if c >= 0. and c < res: res = c
      return np.where(coord == res)[0][0]
   # - Searches for nearest on the positive side
   def nnnegative(coord, stn):
      res = -999.
      coord = coord - stn
      for c in coord:
         if c < 0. and c > res: res = c
      return np.where(coord == res)[0][0]
         
   i = [] # closest grid points latitude
   j = [] # closest grid points longitude

   # - If a station is WEST but less west then
   #   the last point of the earth (on lons, from
   #   the opendap server) we cannot find four
   #   neighbor points. Skip two. 
   if stationlon < np.max(obj.lon): 
      # - North East (upper right) point
      #   lat: find closest positive
      #   lon: find closest positive
      i.append(nnpositive(obj.lat,stationlat))
      j.append(nnpositive(obj.lon,stationlon))
      # - South East (lower right) point
      #   lat: find closest negative
      #   lon: find closest positive
      i.append(nnnegative(obj.lat,stationlat))
      j.append(nnpositive(obj.lon,stationlon))

   # - South West (lower left) point
   #   lat: find closest negative
   #   lon: find closest negative
   i.append(nnnegative(obj.lat, stationlat))
   j.append(nnnegative(obj.lon, stationlon))
   # - North West (upper right) point
   #   lat: find closest positive
   #   lon: find closest negative
   i.append(nnpositive(obj.lat, stationlat))
   j.append(nnnegative(obj.lon, stationlon))

   # User output ...
   if len(i) == 2:
      print("      - NW:  {0:5.2f}/{1:5.2f}".format(
           obj.lat[i[0]], obj.lon[j[0]]))
      print("      - SW:  {0:5.2f}/{1:5.2f}".format(
           obj.lat[i[0]], obj.lon[j[0]]))
   else:
      print("      - NW and NE:  {0:5.2f}/{1:5.2f}   {2:5.2f}/{3:5.2f}".format(
           obj.lat[i[3]], obj.lon[j[3]], obj.lat[i[0]], obj.lon[j[0]]))
      print("      - SW and SE:  {0:5.2f}/{1:5.2f}   {2:5.2f}/{3:5.2f}".format(
           obj.lat[i[2]], obj.lon[j[2]], obj.lat[i[1]], obj.lon[j[1]]))

   return i, j 



# -------------------------------------------------------------------
# - Exit handler
# -------------------------------------------------------------------
def exit(msg=''):
   raise Exception(msg)



















