# -------------------------------------------------------------------
# - NAME:        main.py
# - AUTHOR:      Reto Stauffer
# - DATE:        2015-01-15
# -------------------------------------------------------------------
# - DESCRIPTION:
# -------------------------------------------------------------------
# - EDITORIAL:   2015-01-15, RS: Created file on pc24-c707.
# -------------------------------------------------------------------
# - L@ST MODIFIED: 2019-01-31 20:24 on marvin
# -------------------------------------------------------------------




def __getattr__(self, key):
    if key.startswith('__') and key.endswith('__'):
        return super(DictionaryLike, self).__getattr__(key)
    return self.__getitem__(key)


# -------------------------------------------------------------------
# - Main part
# -------------------------------------------------------------------
if __name__ == '__main__':

   # - Has to be on top to work!
   import matplotlib
   matplotlib.use('Agg')

   import sys, os
   sys.path.append("PyModules")
   import socket
   import utils
   import inputcheck
   import readconfig
   from pydap.client import open_url
   from datetime import datetime as dt
   import numpy as np

   # - GFS ENSEMBLE: GENS visualizer
   from GENSdap import GENSdap

   import pydap.lib

   os.environ['TZ'] = 'UTC'

   # ----------------------------------------------------------------
   # - Parsing input arguments, reading config file and
   #   initialize the GFSdownloader object.
   # ----------------------------------------------------------------
   inputs = inputcheck.inputcheck()

   # - I created hostname dependent config files. Reason: so a can
   #   easily checkout the repository on different machines without
   #   changing the config.conf file every time.
   #   Fallback (if no hostname dependent config is around) is the
   #   config.conf file.
   configfile = "GENS_config.conf"
   if os.path.isfile('%s_GENS_config.conf' % socket.gethostname()):
      configfile = '%s_GENS_config.conf' % socket.gethostname()
   else:
      print '   - No host specific config file %s -> fallback to GENS_config.conf' % \
             '%s_GENS_config.conf' % socket.gethostname()
   config = readconfig.readconfig(configfile,inputs,True)

   if config['caching']:
      pydap.lib.CACHE = "/tmp/pydap-cache/"


   # ----------------------------------------------------------------
   # - Initializing the class.
   #   Also opens opendap connection, loads some data and defines
   #   which is the newest run (if there are no user inputs set on
   #   -d/--date and -r/--runhour or what it is). 
   # ----------------------------------------------------------------
   obj = GENSdap(config)

   # Devel mode. Shows available variable names
   ###obj = GENSdap(config,showvarlistonly=True)


   # ----------------------------------------------------------------
   # - Screen info
   # ----------------------------------------------------------------
   for k in config['parameter']:
      print '    I have to visualize %s' % k


   # ----------------------------------------------------------------
   # - Find nearest neighbors and stuff 
   # ----------------------------------------------------------------
   obj.nearest_neighbors()
   # ----------------------------------------------------------------


   from worker import worker
   from multiprocessing import Pool
   import subprocess

   # ----------------------------------------------------------------
   # - Crete data list object with the necessary infos
   #   we need to start the multiprocess worker.
   # ----------------------------------------------------------------
   parallel_data = []
   for param in config['parameter']:
      tmp = {}
      tmp['param'] = param
      tmp['box'] = obj.box
      tmp['t'] = [obj.tmin,obj.tmax]
      tmp['m'] = [obj.mmin,obj.mmax]
      tmp['data'] = obj.dap[param].data[0]
      parallel_data.append( tmp )


   # ----------------------------------------------------------------
   # - Parallel?
   # ----------------------------------------------------------------
   if config['parallel'] > 1:
      # ---------------- parallel downloader -------------------
      # - Create multiprocess: downloading parallel if
      #   config['parallel'] set > 1. We will store the result (which
      #   is "True" if no problems during download process or "False"
      #   if there as a non zero exit status from the curl command
      #   into the results list. 
      print '--pooling now--'
      pool = Pool( processes=config['parallel'] )
      results = []
      r = pool.map_async(worker, parallel_data, callback=results.append)
      r.wait() # Wait on the results
      print '--pooling end--'
      errors = 0
      for elem in results:
         if elem: errors = errors + 1
      if not r.successful():
         print r._value
         utils.exit('multiprocess not successful')
      for rec in results[0]:
         obj.interpolate( rec )

      # ---------------- parallel downloader -------------------

   # ----------------------------------------------------------------
   # ... or sequential?
   # ----------------------------------------------------------------
   else:
      # --------------- sequential downloader ------------------
      # - Loading data. I should put them into a multiprocess
      #   somewhen.
      for pd in parallel_data:
         print ' * Processing data for %s' % param
         print '   Loading data from the opendap server ...'
         data   = worker( pd )
         print '   Interpolate the values ...'
         ipdata = obj.interpolate( data )
      # --------------- sequential downloader ------------------
         

   # ----------------------------------------------------------------
   # - Data manipulation. This thing is searching for
   #   the data keys and manipulates the data. As an example:
   #   if ugrd10m and vgrd10m are present the method kicks
   #   out these two but adds a ff10m. Or another example:
   #   converting temperatures from Kelvin to Celsius.
   #   Warning: only data in obj.data_container will be
   #   checked.
   # ----------------------------------------------------------------
   obj.dataprocessing() 


   # ----------------------------------------------------------------
   # - If datadir in config file was set (and directory exists)
   #   write the data into an ascii file.
   # ----------------------------------------------------------------
   obj.write_asciifiles()

   print obj.data_container.keys()
   print "WARNING KICK TMAX OUT - WAS JUST FOR JAKOB"
   if 'tmax2m' in obj.data_container.keys():
      del obj.data_container['tmax2m']

      
   # - ugrd10m and vgrd10m automatically create ff10m and dd10m.
   #   We dont need dd10m here. Skip.
   if 'dd10m' in obj.data_container.keys():
      del obj.data_container['dd10m']

   # ----------------------------------------------------------------
   # - Plotting the object. Needs some data preparation
   #   and reogranization of the data structure which is
   #   done internally. Usng RC_functions and RC_plotting
   #   written (originally) by Felix Schueller.
   # ----------------------------------------------------------------
   for s in range(0,len(obj.stations)):
      obj.plot(s,'leps')
      obj.plot(s,'eps')


   # ----------------------------------------------------------------
   # - Write entry into the checkfile
   # ----------------------------------------------------------------
   obj.create_checkfile()


   if socket.gethostname() == 'pc24-c707':

      os.system('rsync -vart pngs/* retos@ertel2.uibk.ac.at:/var/www/html/ertel/data/pngs/meteogramonline/')

else:
   print "This script has to be started as main script, not as a module!"
